<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>E-Trade Latinoamérica / pago a proveedores</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- jQuery first, then Bootstrap JS. -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>

        <style>
            #header {background-color: #3e3d44; color:#ff8500; padding: 10px; margin-top: 10px;}
            #menu   {background-color: #ff8500; color:white; padding: 8px;}
            #menu a {color: white;}
            #main   {padding: 10px;}
            #week   {color: black; width: 100%; margin-bottom: 10px;}
            .card   {border: 2px solid #e5e5e5;}
        </style>
    </head>
    <body>

        <div class="container-fluid">
            <div id="header" class="card">
                <h1>E-Trade Latinoamérica / pago a proveedores.</h1>
            </div>
            <div id="main" class="card">
                @yield('content')
            </div>
        </div>
    </body>
</html>