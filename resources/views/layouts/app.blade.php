<?
use App\TransactionBatch;
/* @var $this \Illuminate\View\Engines\CompilerEngine */

/**
 * Generates the link and make it active if the path is right
 *
 * @param string $to    Path
 * @param string $label What the link should say
 */
function activeLink($to, $label) {
    $link = '<a class="nav-link ';
        if (Request::is(substr($to, 1))) {
            $link .= 'active';
        }
    $link .='" href="'.$to.'">';
    $link .= $label;
    $link .= '</a>';
    return $link;
}
?>
<!-- resources/views/layouts/app.blade.php -->
<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- Required meta tags always come first -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>E-Trade Latinoamérica / pago a proveedores</title>

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

        <!-- jQuery first, then Bootstrap JS. -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.2.0/js/tether.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.2/js/bootstrap.min.js"></script>

        <style>
            #header {background-color: #3e3d44; color:#ff8500; padding: 10px; margin-top: 10px;}
            #menu   {background-color: #ff8500; color:white; padding: 8px;}
            #menu a {color: white;}
            #main   {padding: 10px;}
            #week   {color: black; width: 100%; margin-bottom: 10px;}
            .card   {border: 2px solid #e5e5e5;}
        </style>
    </head>
    <body>

        <div class="container-fluid">
            <div id="header" class="card">
                <h1>E-Trade Latinoamérica / pago a proveedores.</h1>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <div class="card"  id="menu">
                        <h3>Menu</h3>
                        <div class="text-center">
                            <select id="week">
                                @foreach ($globalBatches as $batch)
                                    <option value="{{ $batch->id }}"
                                        @if ($batch->id == session(TransactionBatch::SESSION_GLOBAL_BATCH_ID)) {!! 'selected="selected"' !!} @endif
                                    >
                                        - {{ $batch->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <ul class="nav nav-pills nav-stacked  ">
                            <li class="nav-item"><?=activeLink('/batch', 'Listado de establecimientos.') ?></li>
                            <li class="nav-item"><?=activeLink('/batch/upload', 'Importar nueva semana.') ?></li>
                            <li class="nav-item"><?=activeLink('/batch/banks', 'Exportar pago a bancos.') ?></li>
                            <li class="nav-item"><?=activeLink('/batch/list', 'Mantenimiento de semanas.') ?></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-9 ">
                    @if (Session::has('flash_success'))
                        <div class="alert alert-success alert-dismissible fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          {!! Session::get('flash_success') !!}
                        </div>
                    @endif
                    @if (Session::has('flash_error'))
                        <div class="alert alert-warning alert-danger fade in" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                          {!! Session::get('flash_error')!!}
                        </div>
                    @endif
                    <div id="main" class="card">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <script>
            $(function() {
                $('body').on('change', '#week', function() {
                    var url = '/batch/set-global/' + $(this).val();
                    document.location = url;
                });
            });
        </script>
    </body>
</html>