<!-- resources/views/tasks.blade.php -->

@extends('layouts.print')

@section('content')
    <style>
    .longText {
        overflow:hidden !important;
        text-overflow: ellipsis;
    }
    </style>
    <h2>
        {{$merchant->name}}
        <i><small><small>{{$transactions[0]->merchant_email}}</small></small></i>
    </h2>


    <div class="row">
        <div class="col-sm-5">
            <dl>
                <dt>Dirección fiscal:</dt>   <dd>{{$transactions[0]->merchant_address}}</dd>
                <dt>NIT:</dt>                <dd>{{$transactions[0]->nit}}</dd>
            </dl>
        </div>
        <div class="col-sm-3">
            <dl>
                <dt>Número de cuenta:</dt>   <dd>{{$transactions[0]->bank_number}}</dd>
                <dt>Banco:</dt>              <dd>{{$transactions[0]->bank_name}}</dd>
            </dl>
        </div>

        <div class="col-sm-4">
            <dl>
                <dt>Monto a facturar:</dt>              <dd>Q. {{number_format($totals['invoice'], 2)}}</dd>
                <dt>Monto neto por cupones usados:</dt> <dd>Q. {{number_format($totals['coupons'], 2)}}</dd>
                <dt>Total a pagar:</dt>                 <dd>Q. {{number_format($totals['toPay'], 2)}}</dd>
            </dl>
        </div>
    </div>
    <br />
    <table class="table  table-striped table-sm">
        <thead>
            <tr>
                <th>fecha</th>
                <th>pedido</th>
                <th>cupón</th>
                <th>producto</th>
                <th>% comisión</th>
                <th>comisión</th>
                <th>comercio</th>
                <th>total</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($transactions as $transaction)
            <tr >
                <td>{{ $transaction->order_date }}</td>
                <td>{{ $transaction->transaction_id }}</td>
                <td class="longText" title="{{ $transaction->cupon_code }}">{{ $transaction->coupon_code}}</td>
                <td class="small">{{ $transaction->product_name}}</td>
                <td class="text-sm-right">{{ $transaction->commission}} %</td>
                <td class="text-sm-right">Q. {{ number_format($transaction->commissionAmount, 2)}}</td>
                <td class="text-sm-right">Q. {{ number_format($transaction->merchantAmount, 2)}}</td>
                <td class="text-sm-right">Q. {{ number_format($transaction->total , 2)}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection