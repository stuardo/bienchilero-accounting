<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')
    <h2>Importar nuevo archivo CSV.</h2>
    <form enctype="multipart/form-data" method="POST">
        <fieldset>
            <div class="form-group row">
                <label class="col-sm-4 form-control-label">Seleccione el archivo CSV:</label>
                <div class="col-sm-8">
                    <input class="form-control form-control-file" type="file" name="csv" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-offset-4 col-sm-8">
                 {{ csrf_field() }}
                  <button type="submit" class="btn btn-primary">cargar</button>
                </div>
            </div>
        </fieldset>
    </form>

@endsection