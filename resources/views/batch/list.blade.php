<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')
    <script>
    $(function() {
        $('.delete').click(function() {
            var id = $(this).data('id');
            return window.location = '/batch/del/' + id;
        });
    });

    </script>
    <style>
    tbody {
        height:  400px;

        display: block;
        overflow: auto;
    }
    thead, tbody tr {
        display:      table;
        width:        100%;
        table-layout: fixed; /* even columns width , fix width of table too*/
    }
    th {
        text-align: center;
    }
    thead {
        width: calc( 100% ); /* - 1em : scrollbar is average 1em/16px width, remove it from thead width */
    }
    </style>

    <h2>Listado de semanas importadas</h2>
        <table class="table  table-striped table-hover table-sm">
        <thead class="thead-inverse">
            <tr>
                <th>nombre</th>
                <th>inicio</th>
                <th>fin</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($batches as $batch)
            <tr>
                <td>{{ $batch->name }}</td>
                <td>{{ $batch->starts }}</td>
                <td>{{ $batch->ends }}</td>
                <td>
                    <button class="btn btn-sm btn-danger delete" data-id="{{ $batch->id }}">
                        <i class="fa fa-trash-o"></i> borrar
                    </button>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection