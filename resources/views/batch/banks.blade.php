<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')
    <style>
    .card .card { padding: 10px;}
    tbody {
        max-height:  400px;
        display: block;
        overflow: auto;
    }
    thead, tbody tr {
        display:      table;
        width:        100%;
        table-layout: fixed; /* even columns width , fix width of table too*/
    }
    th {
        text-align: center;
    }
    thead {
        width: calc( 100% ); /* - 1em : scrollbar is average 1em/16px width, remove it from thead width */
    }
    </style>

    <h2>Listado de pagos aprobados para la semana</h2>

    <div class="card">
        <h4>Pagos manuales</h4>
        <table class="table  table-striped table-hover table-sm">
            <thead class="thead-inverse">
                <tr>
                    <th>nombre</th>
                    <th>banco</th>
                    <th>número de cuenta</th>
                    <th>total</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($payments['manual'] as $payment)
                <tr>
                    <td><a href="/batch/merchant/{{ $payment['merchant_id'] }}">{{ $payment['merchant'] }}</a></td>
                    <td>{{ $payment['bank_name']}}</td>
                    <td>{{ $payment['bank_number']}}</td>
                    <td class="text-sm-right">Q. {{ number_format($payment['total'], 2)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <hr  />

    <div class="card">
        <h4>Pagos automáticos</h4>
        <table class="table  table-striped table-hover table-sm">
            <thead class="thead-inverse">
                <tr>
                    <th>nombre</th>
                    <th>banco</th>
                    <th>número de cuenta</th>
                    <th>total</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($payments['csv'] as $payment)
                <tr>
                    <td><a href="/batch/merchant/{{ $payment['merchant_id'] }}">{{ $payment['merchant'] }}</a></td>
                    <td>{{ $payment['bank_name']}}</td>
                    <td>{{ $payment['bank_number']}}</td>
                    <td class="text-sm-right">Q. {{ number_format($payment['total'], 2)}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <div class="text-sm-center">
            <a class="btn btn-primary" href="/batch/banks/export"><i class="fa fa-download"></i>
                generar CSV de pagos
            </a>
        </div>
    </div>
@endsection