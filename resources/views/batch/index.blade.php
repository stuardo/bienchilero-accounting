<!-- resources/views/tasks.blade.php -->

@extends('layouts.app')

@section('content')
    <style>
    tbody {
        height:  400px;

        display: block;
        overflow: auto;
    }
    thead, tbody tr {
        display:      table;
        width:        100%;
        table-layout: fixed; /* even columns width , fix width of table too*/
    }
    th {
        text-align: center;
    }
    thead {
        width: calc( 100% ); /* - 1em : scrollbar is average 1em/16px width, remove it from thead width */
    }
    </style>

    <h2>Listado de establecimientos</h2>
        <table class="table  table-striped table-hover table-sm">
        <thead class="thead-inverse">
            <tr>
                <th>nombre</th>
                <th>aprobado</th>
                <th>notificado</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($merchants as $merchant)
            <tr>
                <td><a href="/batch/merchant/{{ $merchant->id }}">{{ $merchant->name }}</a></td>
                <td class="text-sm-center">
                    @if ($merchant->approved)
                    <i class="fa fa-check text-success"></i>
                    @else
                    <i class="fa fa-circle-o text-warning"></i>
                    @endif
                </td>
                <td class="text-sm-center">
                    @if ($merchant->notified)
                    <i class="fa fa-check text-success"></i>
                    @else
                    <i class="fa fa-circle-o text-warning"></i>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <div class="text-sm-center">
        <a class="btn btn-primary" href="/batch/notify"><i class="fa fa-envelope-o"></i> enviar notificaciones pendientes</a>
    </div>
@endsection