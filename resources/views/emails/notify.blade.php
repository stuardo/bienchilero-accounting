<p>Estimado Proveedor:</p>

<p>
    Etrade Latinoamérica  desea informarle que según el corte correspondiente a
    la semana de {{ $batch->starts }} al {{ $batch->ends }} (Redención
    {{ $batch->id }}), se le acredito a su cuenta el monto de los cupones
    ingresados.
</p>

<p>Adjunto encontrará el detalle de cupones pagados</p>

<p>Cualquier duda y/o comentario al respecto estamos a la orden.</p>
<p>Saludos!!!</p>
<p>¡Qué tengas un día <span style="color: #ff9900">Bienchilero</span>!</p>