<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionBatchDetail extends Model
{
    protected $table    = 'transaction_batch_detail';
    // protected $dates = ['order_date', 'record_date'];
    protected $guarded  = [];
    public $timestamps  = false;

    public function transactionBatch()
    {
        return $this->belongsTo('TransactionBatch');
    }
}
