<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use App\TransactionBatch;
use App\MerchantStatus;
use App\TransactionBatchDetail;
use App\InterBanco;

class BatchController extends Controller
{
    public function getIndex()
    {
        $batchId   = session(TransactionBatch::SESSION_GLOBAL_BATCH_ID);
        if (!$batchId) {
            $batches = TransactionBatch::orderBy('starts', 'desc')->get();
            if (count($batches)) {
                session([TransactionBatch::SESSION_GLOBAL_BATCH_ID => $batches[0]->id]);
                $batchId = $batches[0]->id;
            }
        }
        $merchants = MerchantStatus::query()->where('transaction_batch_id', $batchId)->get();
        return view('batch/index', compact('merchants'));
    }

    public function getList()
    {
        $batches = TransactionBatch::orderBy('starts', 'desc')->get();
        return view('batch/list', compact('batches'));
    }

    public function getDel($id)
    {
        TransactionBatch::destroy($id);
        \Session::flash('flash_success', '<strong>Éxito:</strong> La semana cargada ha sido eliminada.');
        return redirect('/batch/list');
    }

    /**
     * List merchant transactions
     *
     * @param int $id
     */
    public function getMerchant($id, $pdf = false)
    {
        $merchant     = MerchantStatus::find($id);
        $transactions = TransactionBatchDetail::where([
            'merchant_name'        => $merchant->name,
            'transaction_batch_id' => $merchant->transaction_batch_id,
        ])->get();
        $batch  = TransactionBatch::find($merchant->transaction_batch_id);
        $totals = [
            'invoice' => 0,
            'coupons' => 0,
            'toPay'   => 0,
        ];
        foreach ($transactions as $key => $transaction) {
            $commissionAmount  = $transaction->total * $transaction->commission / 100 ;
            $merchantAmount    = $transaction->total - $commissionAmount;

            $totals['invoice'] += $commissionAmount;
            $totals['coupons'] += $transaction->total;
            $totals['toPay']   += $merchantAmount;
            $transactions[$key]->commissionAmount = $commissionAmount;
            $transactions[$key]->merchantAmount   = $merchantAmount;
        }
        $pdfUrl = $merchant->getDetailsPdfUrl($batch);
        $view   = ($pdf) ? 'batch/detail-pdf' : 'batch/detail';
        return view($view, compact('merchant', 'pdfUrl','transactions', 'totals'));
    }

    public function postMerchant($id)
    {
        $merchant = MerchantStatus::find($id);
        $merchant->approved = !$merchant->approved;
        $merchant->save();
        \Session::flash('flash_success', '<strong>Éxito:</strong> Se actualizó el estado del comercio.');
        return redirect('/batch/merchant/' . $id);
    }

    public function getUpload()
    {
        return view('batch/upload');
    }

    public function postUpload(Request $request)
    {
        if (!$request->hasFile('csv')) {
            \Session::flash('flash_error', '<strong>Error</strong>: No se pudo cargar el archivo CSV.');
        } else {
            if ($request->file('csv')->isValid()) {
                $csv   = $request->file('csv');
                /* @var $csv \Illuminate\Http\UploadedFile */
                $batch    = new TransactionBatch();
                $errorMsg = '';
                $success  = false;
                try {
                    $success = $batch->upload($csv->getPathname());
                } catch (\Exception $e){
                    $errorMsg = $e->getMessage();
                }
                if ($success) {
                    $this->getSetGlobal($batch->id);
                    \Session::flash('flash_success', '<strong>Éxito:</strong>. El CSV se cargó con éxito.');
                } else {
                    \Session::flash('flash_error', '<strong>Error</strong>: No se pudo cargar el archivo CSV. '.$errorMsg);

                }
            }
        }
        return redirect('/batch/upload');
    }

    public function getSetGlobal($id)
    {
        $id = (int) $id;
        session([TransactionBatch::SESSION_GLOBAL_BATCH_ID => $id]);
        \Session::flash('flash_success', '<strong>Éxito:</strong>. Se seleccionó una nueva semana.');
        return redirect('/batch');
    }

    public function getNotify()
    {
        $batchId   = session(TransactionBatch::SESSION_GLOBAL_BATCH_ID);
        $merchants = MerchantStatus::query()
            ->where('transaction_batch_id', $batchId)
            ->where('approved', true)
            ->where('notified', false)
            ->get();

        foreach ($merchants as $merchant) {
            $batch      = TransactionBatch::find($merchant->transaction_batch_id);
            $detail     = TransactionBatchDetail::query()
                ->where('transaction_batch_id', $merchant->transaction_batch_id)
                ->where('merchant_name', $merchant->name)
                ->get()[0];
            $attachName = $merchant->getPdfName($batch);
            $pdfUrl     = $merchant->getDetailsPdfUrl($batch);
            // $pdfUrl  = 'http://www.html2pdf.it/?url=http://demo2.maphpia.com/batch/merchant/261/pdf&orientation=landscape&zoom=0.65&download=true&filename=about_nutrition-2016-02-22';
            $mailData = compact('batch', 'merchant', 'pdfUrl', 'attachName', 'detail');
            \Mail::send('emails.notify', $mailData, function ($m) use ($mailData) {
                $fromSubject = "Pago {$mailData['merchant']->name} RED-{$mailData['merchant']->transaction_batch_id}";
                $m->from(env('MAIL_FROM_MAIL'), env('MAIL_FROM_NAME'));
                $m->to($mailData['detail']->merchant_email, $mailData['detail']->merchant_name)->subject($fromSubject);
                $m->attach($mailData['pdfUrl'], ['as' => $mailData['attachName']]);
            });
            $merchant->notified = true;
            $merchant->save();
        }
        \Session::flash('flash_success', '<strong>Éxito:</strong> Notificaciones enviadas.');
        return redirect('batch');
    }

    public function getBanks($export = false)
    {
        $batchId = session(TransactionBatch::SESSION_GLOBAL_BATCH_ID);
        $bank    = new InterBanco();
        if (!$export) {
            $payments = $bank->generatePayments($batchId, $this);
            return view('batch/banks', compact('payments'));
        } else {
            $bank->generateTransfers($batchId, $this);
            die();
        }
    }

}
