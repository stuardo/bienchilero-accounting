<?php
namespace App;

class InterBanco
{
    private $file;

    const ACCOUNT_TYPE_MONETARIOS = 0;
    const ACCOUNT_TYPE_AHORROS    = 1;
    const ACCOUNT_TYPE_TCREDITO   = 2;
    const ACCOUNT_TYPE_PRESTAMO   = 3;

    private $bankCode = [
        'CHN'      => 104,
        'BANTRAB'  => 112,
        'BAM'      => 114,
        'BI'       => 115,
        'BANRURAL' => 116,
        'BAC'      => 142,
        'CITI'     => 143,
        'GyT'      => 145,
    ];

    public function __construct($path = 'php://output')
    {
        $this->file = fopen($path, 'w');
    }


    /**
     * Generates payments for manual and review
     */
    public function generatePayments($batchId, $batchController)
    {

        $merchants = MerchantStatus::query()
            ->where('transaction_batch_id', $batchId)
            ->where('approved', true)
            ->get();

        $payments = ['csv' => [], 'manual' => []];
        foreach ($merchants as $merchant) {
            $view   = $batchController->getMerchant($merchant->id);
            $total  = $view->getdata()['totals']['toPay'];
            $detail = $view->getdata()['transactions'][0]->getAttributes();
            $group  = ( isset($this->bankCode[$detail['bank_name']]) ) ? 'csv' : 'manual';
            $payments[$group][] = [
                'merchant_id' => $merchant->id,
                'merchant'    => $detail['merchant_name'],
                'bank_name'   => $detail['bank_name'],
                'bank_number' => preg_replace('/[^0-9]/', '', $detail['bank_number']),
                'total'       => $total,
            ];
        }
        return $payments;
    }

    /**
     * Generates transfer batch file
     *
     * - código de banco
     * - código de producto
     * - código tipo de persona = 1
     * - nombre
     * - apellido
     * - núermo de cuenta
     * - concepto = PagoRED-#batcId
     * - monto
     */
    public function generateTransfers($batchId, $batchController)
    {
        $merchants = MerchantStatus::query()
            ->where('transaction_batch_id', $batchId)
            ->where('approved', true)
            ->get();

        $filename = "pagos-red-$batchId.csv";
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment;filename='.$filename);
        foreach ($merchants as $merchant) {
            $view   = $batchController->getMerchant($merchant->id);
            $total  = $view->getdata()['totals']['toPay'];
            $detail = $view->getdata()['transactions'][0]->getAttributes();
            $type   = ( (stripos($detail['bank_name'], 'ahorro') === false) &&
                        (stripos($detail['bank_number'], 'ahorro') === false))
                    ? self::ACCOUNT_TYPE_MONETARIOS
                    :  self::ACCOUNT_TYPE_AHORROS;
            if (! isset($this->bankCode[$detail['bank_name']]) ) continue;
            fputcsv($this->file, [
                $this->bankCode[$detail['bank_name']],
                $type, 1,
                $detail['merchant_name'], '',
                preg_replace('/[^0-9]/', '', $detail['bank_number']),
                'Pago RED-'.$batchId,
                $total,
            ]);
        }
        fclose($this->file);
    }
}