<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class TransactionBatch extends Model
{
    const SESSION_GLOBAL_BATCH_ID = 'globalBatchId';

    protected $table   = 'transaction_batch';
    public $timestamps = false;

    public function details()
    {
        return $this->hasMany('TransactionBatchDetail');
    }

    public function upload($csvPath)
    {
        DB::beginTransaction();
        $success = false;
        if (($handle = fopen($csvPath, 'r')) == FALSE) {
            throw new \Exception('No se pudo leer el archivo CSV.');
        } else {
            $header  = true;
            $details = $merchant = [];
            $maxDate = $minDate  = false;
            while (($data = fgetcsv($handle, 1000, ',')) !== FALSE) {
                if (count($data) != 17) {
                    throw new \Exception('Archivo CSV no tiene el formato definido,');
                }
                if ($header) {
                    $header = !$header;
                    continue;
                }

                $recordDate = \DateTime::createFromFormat('Y-m-d H:i:s', $data[4]);
                if (!$recordDate) continue;

                $data = [
                        'transaction_id'   => $data[0],
                        'order_date'       => \DateTime::createFromFormat('d/m/Y H:i:s', $data[1]),
                        'buyer_email'      => $data[2],
                        'coupon_code'      => $data[3],
                        'record_date'      => $recordDate,
                        'campain_id'       => $data[5],
                        'product_name'     => $data[6],
                        'deal_type'        => $data[7],
                        'sales_person'     => $data[8],
                        'merchant_name'    => trim($data[9]),
                        'merchant_email'   => $data[10],
                        'commission'       => $data[11],
                        'total'            => $data[12],
                        'merchant_address' => $data[13],
                        'bank_number'      => $data[14],
                        'bank_name'        => $data[15],
                        'nit'              => $data[16],
                ];
                $merchant[$data['merchant_name']] = $data['merchant_name'];
                if (!$minDate) {
                    $maxDate = $minDate = $data['order_date'];
                }
                if ($minDate > $data['order_date']) $minDate = $data['order_date'];
                if ($maxDate < $data['order_date']) $maxDate = $data['order_date'];
                $details[] = $data;
            }
            fclose($handle);
            sort($merchant);

            $this->starts = $minDate;
            $this->ends   = $maxDate;
            $this->name   = "semana {$minDate->format('d/m')} - {$maxDate->format('d/m')}";
            if (!$this->save()) {
                throw new \Exception('No se pudo guardar el contenido en la base de datos.');
            }
            foreach ($details as $data) {
                $data['transaction_batch_id'] = $this->id;
                $detail = new TransactionBatchDetail($data);
                $detail->save();
            }
            foreach ($merchant as $data) {
                $status                       = new MerchantStatus();
                $status->name                 = $data;
                $status->transaction_batch_id = $this->id;
                $status->save();
            }
            DB::commit();
            $success = true;
        }
        return $success;
    }
}
