<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantStatus extends Model
{
    protected $table    = 'merchant_status';
    // protected $dates = ['order_date', 'record_date'];
    protected $guarded  = [];
    public $timestamps  = false;

    public function transactionBatch()
    {
        return $this->belongsTo('App\TransactionBatch');
    }

    public function getPdfName($batch)
    {
        $merchantUrl = strtolower($this->name);
        $merchantUrl = preg_replace(array('/ /', '/\W/'), array('_', ''), $merchantUrl);
        $merchantUrl = urlencode($merchantUrl);
        $merchantUrl = $merchantUrl . '-' . $batch->starts . '.pdf';
        return $merchantUrl;
    }

    /**
     *
     * @todo should not need the batch but the file name
     */
    public function getDetailsPdfUrl($batch)
    {
        $pdfUrl = env('APP_URL') . "/batch/merchant/{$this->id}/pdf"
        . '&orientation=landscape&zoom=0.65&download=true'
                . '&filename=' . $this->getPdfName($batch);
        return 'http://www.html2pdf.it/?url=' . $pdfUrl;
    }
}
