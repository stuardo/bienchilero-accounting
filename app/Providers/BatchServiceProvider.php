<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\View\View;
use App\TransactionBatch;

class BatchServiceProvider extends ServiceProvider
{
    public function boot()
    {
        parent::boot();
        view()->composer('*', function (View $view) {
            $batches = TransactionBatch::orderBy('starts', 'desc')->get();
            /* @var $batches \Illuminate\Database\Eloquent\Collection*/
            $view->with('globalBatches', $batches);

            $batchId = session(TransactionBatch::SESSION_GLOBAL_BATCH_ID);
            if (!$batchId) {
                if (count($batches)) {
                    session([TransactionBatch::SESSION_GLOBAL_BATCH_ID => $batches[0]->id]);
                }
            }
        });
    }

    public function register()
    {
        return false;
    }
}