SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

    DROP TABLE IF EXISTS transaction_batch;
    CREATE TABLE transaction_batch (
        id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , name        varchar(128) NOT NULL DEFAULT ''
        , starts      date         NOT NULL
        , ends        date         NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    DROP TABLE IF EXISTS merchant_status;
    CREATE TABLE merchant_status (
        id            INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , name        varchar(128) NOT NULL DEFAULT ''
        , approved    BOOLEAN      NOT NULL DEFAULT FALSE
        , notified    BOOLEAN      NOT NULL DEFAULT FALSE
        , transaction_batch_id  INT UNSIGNED NOT NULL
        , FOREIGN KEY (transaction_batch_id) REFERENCES transaction_batch(id) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    DROP TABLE IF EXISTS transaction_batch_detail;
    CREATE TABLE transaction_batch_detail (
        id                      INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY
        , transaction_batch_id  INT UNSIGNED NOT NULL
        , transaction_id        INT UNSIGNED NOT NULL
        , order_date            DATE         NOT NULL
        , buyer_email           varchar(128) NOT NULL
        , coupon_code           varchar(12)  NOT NULL
        , record_date           DATE         NOT NULL
        , campain_id            varchar(64)  NOT NULL
        , product_name          varchar(256) NOT NULL
        , deal_type             varchar(16)  NOT NULL
        , sales_person          varchar(32)  NOT NULL
        , merchant_name         varchar(64)  NOT NULL
        , merchant_email        varchar(64)  NOT NULL
        , commission            tinyint UNSIGNED NOT NULL
        , total                 DECIMAL(9,2)
        , merchant_address      varchar(128) NOT NULL
        , bank_number           varchar(64)  NOT NULL
        , bank_name             varchar(64)  NOT NULL
        , nit                   varchar(16)  NOT NULL
        , FOREIGN KEY (transaction_batch_id) REFERENCES transaction_batch(id) ON DELETE CASCADE ON UPDATE CASCADE
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;


SET FOREIGN_KEY_CHECKS=1;
SET AUTOCOMMIT = 1;
COMMIT;